How to build ZXTune
===================

Prerequisites
-------------

- `make` tool
- `boost 1.48+` library
- `qt 4.8+` library (only for `zxtune-qt` application)
- `GCC` toolkit for linux
- `MINGW` toolkit or
- `Microsoft Visual C++ 13.0 Express` with `DirectX SDK` for windows

Environment setup
-----------------

All the build settings specified via `make ... <propname>=<propvalue>` can be stored in optional file `variables.mak` in the project's root and will be used for all builds.
For windows, the optional file `variables.bat` can be used to setup environment for build shell provided by `shell.bat`

Possible targets
----------------

Location of `Makefile` files of main locations are `apps/zxtune123`, `apps/zxtune-qt` and `apps/xtractor` for `zxtune123`,`zxtune-qt` and `xtractor` respectively.

Build results
-------------

If the build is successful, the resulting binaries will be located at the `bin` folder, or possibly inside its sub-folders (depending on its build modes).

Building for Linux with system Boost and Qt libraries installed
---------------------------------------------------------------

Run:
```sh
make system.zlib=1 -C <path to Makefile>
```

Building in a Windows environment
=================================

Common steps for MinGW/MSVC building are:
- choose some directory for the prebuilt environment (`prebuilt.dir` in `variables.mak`)
- choose between the `x86` or the `x86_64` architectures (the `$(arch)` variable mentioned below)
- optionally create `variables.bat`, to set up `make` tool availability

After performing the particular environment setup described above, use `shell.bat` to get console session with required parameters set. To start building run:
```sh
make -C <path to Makefile>
```

Building for MinGW
------------------

- Download the `boost-1.55.0.zip` and `boost-1.55.0-mingw-$(arch).zip` files from http://drive.google.com/folderview?id=0BzfWZ2kQHJsGUG5FNG5FbGpQaUE and unpack them to `$(prebuilt.dir)`
- Download the `qt-4.8.5-mingw-$(arch).zip` file from http://drive.google.com/folderview?id=0BzfWZ2kQHJsGRWJfRFFXR
- Choose some directory for the toolchain (`toolchains.root` in `variables.mak`)
- Download `MinGW` from http://sourceforge.net/projects/mingwbuilds/files/host-windows/releases/4.8.1/ , thread-posix/sjlj and unpack it so that `$(toolchains.root)/Mingw/bin` will contain all the toolchain's binaries.

Building for windows
--------------------

- Choose some directory for prebuilt environment (`prebuilt.dir` in `variables.mak`)
- Choose `x86` or `x86_64` architecture (`$(arch)` variable)
- Download `boost-1.55.0.zip` and `boost-1.55.0-windows-$(arch).zip` files from http://drive.google.com/folderview?id=0BzfWZ2kQHJsGUG5FNG5FbGpQaUE and unpack it to `$(prebuilt.dir)`
- Download `qt-4.8.5-windows-$(arch).zip` file from http://drive.google.com/folderview?id=0BzfWZ2kQHJsGRWJfRFFXR

FAQ
===

Why so cumbersome?
------------------

To support all the features required for a successful project. ZXTune is quite a complex end-user product, so optimizing it for other developers is not a goal.

Why not to use ${BUILDSYSTEMNAME} instead of the archaic `make`?
----------------------------------------------------------------

Short answer: because I'm fully satisfied by the current one and don't want to needlessly waste my time.
If you are an expert in ${BUILDSYSTEMNAME}, and wish to introduce it for the ZXTune project, please contact the maintainer to discuss the requirements and the possible pitfalls.
