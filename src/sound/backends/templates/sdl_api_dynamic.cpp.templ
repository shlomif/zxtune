/**
*
* @file
*
* @brief  SDL subsystem API gate implementation
*
* @author vitamin.caig@gmail.com
*
**/

//local includes
#include "sdl_api.h"
//common includes
#include <make_ptr.h>
//library includes
#include <debug/log.h>
#include <platform/shared_library_adapter.h>
//boost includes
#include <boost/range/end.hpp>

namespace Sound
{
  namespace Sdl
  {
    class LibraryName : public Platform::SharedLibrary::Name
    {
    public:
      virtual std::string Base() const
      {
        return "SDL";
      }
      
      virtual std::vector<std::string> PosixAlternatives() const
      {
        static const std::string ALTERNATIVES[] =
        {
          "libSDL-1.2.so.0",
        };
        return std::vector<std::string>(ALTERNATIVES, boost::end(ALTERNATIVES));
      }
      
      virtual std::vector<std::string> WindowsAlternatives() const
      {
        return std::vector<std::string>();
      }
    };

<TMPL_INCLUDE NAME="api_dynamic.cpp.templ">

    Api::Ptr LoadDynamicApi()
    {
      static const LibraryName NAME;
      const Platform::SharedLibrary::Ptr lib = Platform::SharedLibrary::Load(NAME);
      return MakePtr<DynamicApi>(lib);
    }
  }
}
